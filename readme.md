## nats on ec2

config driven nats cluster on ec2


The CDK code will define a configuration driven [Nats](https://docs.nats.io/) cluster in a targeted environment.

The cluster will consists of the following concerns:

* The Seed Node -- a single instance meant to serve as inital entry point and leader for the `JetStream` cluster.
* Nats Node Asg -- auto scaling group that makes up the members of cluster.  

Quorom is (5) Nodes -- one seed and four nodes.

Server level configurations (stream, authorization, etc) will defined in a separate repo or is TBD 


### Development or Testing

In order to bring up stack for development please check the `Makefile` for instructions or review the `local-runner.ts`.

The project will eventually be driven by a CDK pipeline construct but that is TBD.


### Prerequistes

The following are external dependencies

the Nats cluster network definition will require a VPC's `Name` tag with following mask to exist:

* `fusion-vpc-<TARGETED ENV>` -- required in order to be hosted in a proper subnet. 


