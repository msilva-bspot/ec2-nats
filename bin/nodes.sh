#!/usr/bin/env bash

# use the following to fetch known nats IPs 

STACK_NAME=nats-cluster-dev

echo "nats nodes:"
aws ec2 describe-instances \
     --region=$AWS_DEFAULT_REGION \
     --filter "Name=tag:aws:cloudformation:stack-name,Values=$STACK_NAME" \
     --query "Reservations[].Instances[?State.Name == 'running'].PrivateDnsName[]" \
     --output json


echo "bastion host:"

aws ec2 describe-instances \
     --region=$AWS_DEFAULT_REGION \
     --filter "Name=tag:aws:cloudformation:stack-name,Values=nats-cluster-bastion-dev" \
     --query "Reservations[].Instances[?State.Name == 'running'].PublicIpAddress" \
     --output json
	

