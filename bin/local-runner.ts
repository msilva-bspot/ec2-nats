#!/usr/bin/env node

/* 
 * this is the entry point for a local runner.
 * it should be used for testing or feature dev
 * outside of pipeline workflow
*/

import * as fs from 'fs'

import { CfnOutput, Stack, StackProps } from 'aws-cdk-lib';
import * as cdk from 'aws-cdk-lib';

import { FusionBastion } from '../lib/bastion';
import { NatsCluster } from '../lib/nats';

import * as helper from "@gpn-fusion/fusion-cdk-helpers";


const ENV='dev'
const targetEnv='dev01'

const projectPrefix='nats'

const app = new cdk.App();

new NatsCluster(app, `${projectPrefix}-cluster-${ENV}`, {
  targetEnv: ENV,
 env: {account: helper.account(targetEnv), region: 'us-west-2'},
});

new FusionBastion(app, `${projectPrefix}-bastion-${ENV}`, {
  targetEnv: ENV,
  env: helper.account(targetEnv, true)});

app.synth();
