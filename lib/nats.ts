#!/usr/bin/env node

import * as fs from 'fs'

import { CfnOutput, Stack, StackProps } from 'aws-cdk-lib';
import * as cdk from 'aws-cdk-lib';
import * as ec2 from 'aws-cdk-lib/aws-ec2';
import * as iam from 'aws-cdk-lib/aws-iam';
import * as ssm from 'aws-cdk-lib/aws-ssm';
import * as asg from 'aws-cdk-lib/aws-autoscaling';

import { Construct } from 'constructs';


interface NatsStackProps extends cdk.StackProps {
  targetEnv: string 
}

export class NatsCluster extends Stack {

  private generateUserData(seedNodeIp?: string): string {

    let routeString = seedNodeIp == undefined ? "nats://$localIP" : `nats://${seedNodeIp}`
    let natsConfigPath = '/opt/nats/nats.conf'

    return `
      yum -y install -y https://s3.us-east-1.amazonaws.com/amazon-ssm-us-east-1/latest/linux_amd64/amazon-ssm-agent.rpm
      systemctl enable amazon-ssm-agent;systemctl start amazon-ssm-agent
      yum -y install https://github.com/nats-io/nats-server/releases/download/v2.7.2/nats-server-v2.7.2-amd64.rpm
      mkdir -p /opt/nats/data/nats-server
      mkdir -p /var/log/nats
      InstanceID=$(curl http://169.254.169.254/latest/meta-data/instance-id --silent)
      localIP=$(curl http://169.254.169.254/latest/meta-data/local-ipv4 --silent)
      /usr/local/bin/nats-server -js -c ${natsConfigPath} --server_name $InstanceID --cluster nats://$localIP:6222 --cluster_name JSC --routes ${routeString}:6222 -m 8222
      `
  }

  constructor(scope: Construct, id: string, props: NatsStackProps) {
    super(scope, id, props);

    let ENV = props.targetEnv;
    let nodeCount = scope.node.tryGetContext('nats').nodeCount;

    const vpc_name = `fusion-${ENV.toLowerCase()}`
    const vpc = ec2.Vpc.fromLookup(this,"Vpc", {
      tags: { "Name": vpc_name}
    });

    const natsSg = new ec2.SecurityGroup(this, 'natsSecurityGroup', {
      vpc,
      description: 'Allow ssh access to me',
      allowAllOutbound: true,
      disableInlineRules: true
    });
  
    const natsRole = new iam.Role(this, 'nat-service-role', {
      assumedBy: new iam.ServicePrincipal('ec2.amazonaws.com'),
      description: 'A nats service role',
      managedPolicies: [
        iam.ManagedPolicy.fromAwsManagedPolicyName('AmazonSSMManagedInstanceCore'),
      ],
    });

    natsRole.addToPolicy(
      new iam.PolicyStatement({
        actions: ['logs:CreateLogGroup', 'logs:CreateLogStream'],
        resources: ['*'],
      }),
    );

    natsSg.addIngressRule(
      ec2.Peer.anyIpv4(),
      ec2.Port.tcp(8222), 'acess for monitor port');

    natsSg.addIngressRule(
        ec2.Peer.anyIpv4(),
        ec2.Port.tcp(6222), 'acess for nats port');

    natsSg.addIngressRule(
        ec2.Peer.anyIpv4(),
        ec2.Port.tcp(4222), 'acess for nats port');

    let instanceProps = {
      vpc,
      role: natsRole,
      instanceType: ec2.InstanceType.of(ec2.InstanceClass.BURSTABLE2, ec2.InstanceSize.MICRO),
      machineImage: new ec2.AmazonLinuxImage() ,
      securityGroup: natsSg,
      minCapacity: nodeCount,
      maxCapacity: nodeCount,
      init: ec2.CloudFormationInit.fromElements(
        ec2.InitFile.fromFileInline( "/opt/nats/auth_dev.config", "./config/auth_dev.config"), 
        ec2.InitFile.fromFileInline( "/opt/nats/nats.conf", "./config/nats.conf"),
        ), signals: asg.Signals.waitForAll({ timeout: cdk.Duration.minutes(5), }),
    }

    const gnats_node = new ec2.Instance(this, 'seed-node', instanceProps);

    let seedParam = `/${ENV}/NatsCluster/SeedIp` 
    new ssm.StringParameter(this, 'IP', {
        parameterName: seedParam, 
        stringValue: gnats_node.instancePrivateIp
    });

    gnats_node.addUserData(this.generateUserData());
    const seedNodeIp = gnats_node.instancePrivateIp 

    const natsAsg = new asg.AutoScalingGroup(this, 'node', instanceProps);
    natsAsg.addUserData(this.generateUserData(seedNodeIp));
    
  }
}
