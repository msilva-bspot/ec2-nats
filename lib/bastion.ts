import * as cdk from 'aws-cdk-lib';
import * as ec2 from 'aws-cdk-lib/aws-ec2'
import { Construct } from 'constructs';


const MyIP = process.env.MyIp || undefined;
const AWS_KEYPAIR = 'NATS Cluster Dev' // process.env.AWS_KEPAIR;

interface NatsStackProps extends cdk.StackProps {
  targetEnv: string 
}

export class FusionBastion extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: NatsStackProps) {
    super(scope, id, props);

    let ENV = 'dev'

    let vpc_name = `fusion-${ENV.toLowerCase()}`

    const vpc = ec2.Vpc.fromLookup(this,"Vpc", {
      tags: { "Name": vpc_name}
    })

    const sg=  new ec2.SecurityGroup(this, 'bastionSG', {
      vpc: vpc,
      allowAllOutbound: true,
      securityGroupName: 'Bastion SG',
    });

    const bh = new ec2.BastionHostLinux(this, 'nats-node', {
      
      vpc,
      instanceType: ec2.InstanceType.of(ec2.InstanceClass.T2, ec2.InstanceSize.MICRO),
      securityGroup: sg,
      subnetSelection: { subnetType: ec2.SubnetType.PUBLIC },
      init: ec2.CloudFormationInit.fromElements(
        ec2.InitCommand.shellCommand('sudo yum update -y'),
        ec2.InitCommand.shellCommand('sudo wget https://github.com/nats-io/natscli/releases/download/v0.0.28/nats-0.0.28-amd64.rpm'),
        ec2.InitCommand.shellCommand('sudo yum -y install nats-0.0.28-amd64.rpm'),
        ec2.InitCommand.shellCommand('sudo wget https://github.com/nats-io/nats-top/releases/download/v0.4.0/nats-top-v0.4.0-amd64.rpm'),
        ec2.InitCommand.shellCommand('sudo yum -y install nats-top-v0.4.0-amd64.rpm'),
        // ec2.InitCommand.shellCommand('wget https://raw.githubusercontent.com/nats-io/nsc/master/install.py -O /home/ssm-user/install.py && python install.py'),
        // ec2.InitCommand.shellCommand("echo 'export PATH=$PATH:/home/ssm-user/.nsccli/bin' >> /home/ssm-user/.bashrc && source /home/ssm-user/.bashrc"),
      ),
    });

    if(MyIP) { bh.allowSshAccessFrom(ec2.Peer.ipv4(MyIP)); }

    if(AWS_KEYPAIR) { bh.instance.instance.addPropertyOverride('KeyName', AWS_KEYPAIR) }

  }
}
