.PHONY: synth deploy destroy list bastion diff

list:
	npx aws-cdk --app "npx ts-node --prefer-ts-exts ./bin/local-runner.ts" list 

synth:
	npx aws-cdk --app "npx ts-node --prefer-ts-exts ./bin/local-runner.ts" synth 

deploy:
	npx aws-cdk --app "npx ts-node --prefer-ts-exts ./bin/local-runner.ts" deploy nats-cluster-dev

destroy:
	npx aws-cdk --app "npx ts-node --prefer-ts-exts ./bin/local-runner.ts" destroy fusion-nats-cluster-dev

diff:
	npx aws-cdk --app "npx ts-node --prefer-ts-exts ./bin/local-runner.ts" diff nats-cluster-dev

bastion:
	npx aws-cdk --app "npx ts-node --prefer-ts-exts ./bin/local-runner.ts" deploy fusion-nats-bastion-dev
